require! {
  sharp
  crypto
  fs
  request
}

imgBaseDir = "#__dirname/../www/media"

module.exports.toAuthors = (authors) ->
  if authors.length == 1
    authors.0
  else
    last = authors.pop!
    "#{authors.join ", "} a #last"


module.exports.processImage = processImage = (originalSource, {width, height, quality}:opts) ->
  md5gen = crypto.createHash \md5
  width_txt = width || "_"
  height_txt = height || "_"
  md5 = md5gen.update originalSource .digest \hex
  imgtype = originalSource.split '.' .pop!
  imgname = "#{width_txt}x#{height_txt}.#{imgtype}"
  fullPath = "#imgBaseDir/#md5/#imgname"
  dirExists = fs.existsSync "#imgBaseDir/#md5"
  unless dirExists && fs.existsSync fullPath
    fs.mkdirSync "#imgBaseDir/#md5" if not dirExists
    (err, body) <~ getImageFromDiskOrDownload originalSource, imgtype, md5
    sharp body
      .resize width, height
      .quality quality
      .toFile fullPath
  "#{md5}/#{imgname}"

getImageFromDiskOrDownload = (originalSource, imgtype, md5, cb) ->

  orignalDiskAdress = "#imgBaseDir/#md5/original.#imgtype"
  if fs.existsSync orignalDiskAdress
    fs.readFile orignalDiskAdress, cb
  else
    (err, response, body) <~ request.get originalSource, {encoding: null}
    fs.writeFile orignalDiskAdress, body
    cb err, body

---
title: "Data o znalostech češtiny: Nejtěžší jsou velká písmena"
perex: "Čechům dělá největší problém psaní velkých písmen, nejlépe zvládají vyjmenovaná slova. Ukazují to data uživatelů webu umimecesky.cz, který slouží k procvičování pravopisu. Zkoumá je tým z Fakulty informatiky Masarykovy univerzity v Brně."
description: "Na Masarykově univerzitě vznikl web umimecesky.cz pro procvičování českého pravopisu, který se zároveň učí z chyb uživatelů. Pomáhá pochopit, jak vnímáme jazyk, a učit efektivněji."
authors: ["Jan Boček"]
published: "25. července 2016"
coverimg: https://interaktivni.rozhlas.cz/data/cestina/www/media/decko.jpg
coverimg_note: "Foto CC-BY-SA <a href=' https://www.flickr.com/photos/criminalintent/5403052781/in/photostream/'>Lars Plougmann</a>"
url: "cestina"
libraries: [jquery, highcharts]
---

Na Masarykově univerzitě vznikl web [umimecesky.cz](http://www.umimecesky.cz/) pro procvičování českého pravopisu, který se zároveň učí z chyb uživatelů. Pomáhá pochopit, jak vnímáme jazyk, a učit efektivněji. Vyzkoušet si jej můžete i vy.

<aside class="big">
  <iframe src="https://www.umimecesky.cz/doplnovacka-velka-pismena-geografie/2535" style="height:600px"></iframe>
</aside>

„Naší vizí je, aby každé dítě mohlo pracovat ve svém vlastním tempu a procvičit si látku, dokud ji skutečně nezvládne,“ říká autor projektu Petr Jarušek. „Procvičování se snažíme dělat zábavné a přitom stále efektivní. Proto u nás naleznete cvičení motivovaná tetrisem nebo závody robotů stejně jako klasické diktáty či doplňovačky. Nechceme učitele nahradit, chceme jim přinést užitečný nástroj na procvičování látky a rozvázat jim ruce pro pestřejší formy výuky.“

Pravidla jazyka podle autorů webu tvoří plynulé spektrum: od těch, která by měl opravdu umět každý, až po ta obskurní. Autoři se snaží směřovat procvičování tam, kde je to nejužitečnější – kde se nejvíc chybuje a současně nejde o exotické chytáky.

## Řada chytáků má menší úspěšnost než hod mincí

Velmi nízkou úspěšnost – více než čtvrtinu chybných odpovědí – mají zmíněná velká písmena. Vůbec nejvíce chyb uživatelé webu dělají ve spojení *Pásmo Gazy* (85 %), *Sídliště Antala Staška* (84 %) nebo *Eddie Van Halen* (77 %).

<aside class="big">
  <figure>
    <div class="highcharts uspesnost" style="height:600px"></div>
  </figure>
</aside>

„Zajímavé jsou právě jevy s menší než padesátiprocentní úspěšností,“ vysvětluje Pelánek. „To znamená s úspěšností menší, než když si hodíte mincí. Často jsou to chytáky, které se řídí nějakým dávno zapomenutým pravidlem a musíte si je zapamatovat, třeba spojení *kdo s koho*.“

Odpovědi, kde uživatelé dělají nejvíc chyb, následuje vysvětlení, jakým pravidlem se gramatika v daném případě řídí. Uživatelé webu se tak kromě drilování učí i logiku jazyka.

Nejlépe procvičující zvládají vyjmenovaná slova. I mezi nimi jsou ovšem spojení, u kterých většina udělá chybu. Podle výzkumníků je potíž v tom, že mezi vyjmenovanými slovy se nevyskytují výrazy přejaté z cizích jazyků.

„Vyjmenovaná slova se běžně učí jen pro slova českého původu, přitom umět napsat *systém* je rozhodně důležitější než *Bydžov*,“ říká Pelánek.

## Web využívají hlavně děti

Data vypovídají hlavně o dětech, které se rodnou gramatiku teprve učí. Vývojáři to usuzují z návštěvnosti webu: používá se totiž převážně v dopoledních hodinách, kdy také prudce klesá úspěšnost odpovědí.

<aside class="article-aligned">
  <figure>
    <img src="https://interaktivni.rozhlas.cz/data/cestina/www/media/aktivita.png" width="600" height="440">
  </figure>
  <figcaption>
    Češtinu na webu nejčastěji procvičují školáci. Napovídá tomu trend, kdy nejvíc uživatelů přichází během dopoledne a úspěšnost odpovědí ve stejné době klesá.
  </figcaption>
</aside>

Stejná data ukazují, že učitelé se snaží dětem výuku zpříjemnit. Vedle klasických diktátů nebo doplňování správných odpovědí totiž web nabízí akční hry, kdy hráči soupeří s časem nebo mezi sebou, například „jazykový tetris“ nebo „sestřelování vyjmenovaných slov“. Právě ty jsou nejpopulárnější během dopoledne. Večer uživatelé častěji klikají na klasické diktáty nebo doplňování ze dvou možností.

Samostatně se web věnuje také procvičování čárek. Data ukazují, že jde o jeden z nejobtížnějších mluvnických jevů. Některé z procvičovaných vět nedokázal správně doplnit čárkami téměř nikdo. K nejtěžším patří citace z Čapkovy *Dášeňky* nebo knihy *Jak se co dělá*.

*Červeně jsou zobrazena místa, kam uživatelé webu čárku umístili, ačkoliv tam nepatří. Modře naopak místa, kde na čárku zapomněli. Čím tmavší barva, tím častější tato chyba byla.*

> A<span style='background-color:rgba(255,0,0,0.034042538705302682);'> &nbsp;&nbsp;</span>do<span style='background-color:rgba(255,0,0,0.0085106346763256704);'> &nbsp;&nbsp;</span>třetice<span style='background-color:rgba(255,0,0,0.034042538705302682);'> &nbsp;&nbsp;</span>všeho<span style='background-color:rgba(255,0,0,0.0);'> &nbsp;&nbsp;</span>dobrého<span style='background-color:rgba(255,0,0,0.66382950475340219);'> &nbsp;&nbsp;</span>se<span style='background-color:rgba(255,0,0,0.017021269352651341);'> &nbsp;&nbsp;</span>ten<span style='background-color:rgba(255,0,0,0.0085106346763256704);'> &nbsp;&nbsp;</span>udatný<span style='background-color:rgba(255,0,0,0.0);'> &nbsp;&nbsp;</span>Foxlík<span style='background-color:rgba(255,0,0,0.076595712086931028);'> &nbsp;&nbsp;</span>vypravil<span style='background-color:rgba(255,0,0,0.1106382507922337);'> &nbsp;&nbsp;</span>proti<span style='background-color:rgba(255,0,0,0.0);'> &nbsp;&nbsp;</span>samotnému<span style='background-color:rgba(255,0,0,0.20425523223181608);'> &nbsp;&nbsp;</span>přeukrutnému<span style='background-color:rgba(255,0,0,0.17021269352651339);'> &nbsp;&nbsp;</span>tatarskému<span style='background-color:rgba(255,0,0,0.059574442734279684);'> &nbsp;&nbsp;</span>chánovi<span style='background-color:rgba(0,0,255,0.39999982978730647);'>, &nbsp;</span>který<span style='background-color:rgba(255,0,0,0.017021269352651341);'> &nbsp;&nbsp;</span>se<span style='background-color:rgba(255,0,0,0.0);'> &nbsp;&nbsp;</span>jmenoval<span style='background-color:rgba(255,0,0,0.0085106346763256704);'> &nbsp;&nbsp;</span>Pelichán<span style='background-color:rgba(255,0,0,0.16170205885018771);'> &nbsp;&nbsp;</span>a<span style='background-color:rgba(255,0,0,0.0);'> &nbsp;&nbsp;</span>bydlel<span style='background-color:rgba(255,0,0,0.0085106346763256704);'> &nbsp;&nbsp;</span>tamhle<span style='background-color:rgba(255,0,0,0.0085106346763256704);'> &nbsp;&nbsp;</span>ve<span style='background-color:rgba(255,0,0,0.0085106346763256704);'> &nbsp;&nbsp;</span>Strašnicích.

---

> Pokud<span style='background-color:rgba(255,0,0,0.054054005356751929);'> &nbsp;&nbsp;</span>vím<span style='background-color:rgba(0,0,255,0.54054005356751933);'>, &nbsp;</span>nikdo<span style='background-color:rgba(255,0,0,0.0);'> &nbsp;&nbsp;</span>se<span style='background-color:rgba(255,0,0,0.0);'> &nbsp;&nbsp;</span>dosud<span style='background-color:rgba(255,0,0,0.0);'> &nbsp;&nbsp;</span>nepokusil<span style='background-color:rgba(255,0,0,0.090090008927919879);'> &nbsp;&nbsp;</span>vyzkoumat<span style='background-color:rgba(0,0,255,1);'>, &nbsp;</span>z<span style='background-color:rgba(255,0,0,0.0);'> &nbsp;&nbsp;</span>jakých<span style='background-color:rgba(255,0,0,0.0);'> &nbsp;&nbsp;</span>lidí<span style='background-color:rgba(255,0,0,0.03603600357116795);'> &nbsp;&nbsp;</span>se<span style='background-color:rgba(255,0,0,0.0);'> &nbsp;&nbsp;</span>dělají<span style='background-color:rgba(255,0,0,0.018018001785583975);'> &nbsp;&nbsp;</span>novináři.

## Interaktivní základní škola

Kromě češtiny vznikly na stejné fakultě systémy, které se učí z chyb uživatelů, pro [zeměpis](https://slepemapy.cz/), [biologii](http://www.poznavackaprirody.cz/), [matematiku](https://matmat.cz/) nebo [anatomii](https://anatom.cz/). Data ze slepých map jsme [analyzovali už dříve](http://www.rozhlas.cz/zpravy/data/_zprava/1544916).

<aside class="article-aligned">
  <figure>
    <img src="https://interaktivni.rozhlas.cz/data/cestina/www/media/mapa.png" width="600" height="280">
  </figure>
  <figcaption>
    Mapa ukazuje, jak náročné je pro uživatele najít danou zemi. Nejvyšší úspěšnost má Rusko (95 %), nejnižší tichomořský ostrovní stát Vanuatu (12 %).
  </figcaption>
</aside>

„Základem všech těchto systémů je práce s daty. Na základě toho, jak uživatelé odpovídají, se přizpůsobuje chování systémů i náš vývoj,“ vysvětluje Pelánek. „Práce s daty učí člověka pokoře: velmi dobře a velmi často vidíme, jak se intuice mýlí.“
